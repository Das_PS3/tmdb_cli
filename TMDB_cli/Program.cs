﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace TMDB_cli
{
    class Program
    {
        private static readonly byte[] tmdbKey = { 0xF5, 0xDE, 0x66, 0xD2, 0x68, 0x0E, 0x25, 0x5B, 0x2D, 0xF7, 0x9E, 0x74, 0xF8, 0x90, 0xEB, 0xF3, 0x49, 0x26, 0x2F, 0x61,
            0x8B, 0xCA, 0xE2, 0xA9, 0xAC, 0xCD, 0xEE, 0x51, 0x56, 0xCE, 0x8D, 0xF2, 0xCD, 0xF2, 0xD4, 0x8C, 0x71, 0x17, 0x3C, 0xDC, 0x25, 0x94, 0x46, 0x5B,
            0x87, 0x40, 0x5D, 0x19, 0x7C, 0xF1, 0xAE, 0xD3, 0xB7, 0xE9, 0x67, 0x1E, 0xEB, 0x56, 0xCA, 0x67, 0x53, 0xC2, 0xE6, 0xB0 };

        static void Main(string[] args)
        {
            if (args.Length != 1 || !File.Exists(args[0]))
            {
                DisplayError();
                return;
            }

            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; }); // Disable SSL certificate validation

            var results = new List<string>();
            string[] titleIDs = File.ReadAllLines(args[0]);
            for (int i = 0; i < titleIDs.Length; i++)
            {
                string titleID = titleIDs[i].Substring(0, 9);

                Console.WriteLine(string.Format("Processing {0} ({1}/{2})", titleID, i + 1, titleIDs.Length));

                if (titleID.Length != 9 || !Regex.IsMatch(titleID, @"^[A-Z]{4}[\d]{5}$"))
                    continue;

                byte[] seed = Encoding.UTF8.GetBytes(titleID + "_00");
                string hashedSeed = Convert(seed);

                string url = string.Format("http://tmdb.np.dl.playstation.net/tmdb/{0}_00_{1}/{0}_00.xml", titleID, hashedSeed);

                string name = FetchXML(url);
                if (string.IsNullOrEmpty(name))
                    name = FetchUpdateTitle(titleID);
                results.Add(titleID + " " + name);
            }

            File.WriteAllLines(args[0] + "_TMDB.txt", results);
        }

        private static void DisplayError()
        {
            Console.WriteLine("TMDB_cli v1.1.0.1 written by Dasanko for catalinnc");
            Console.WriteLine();
            Console.WriteLine("Usage:");
            Console.WriteLine("TMDB_cli.exe list_of_titleIDs.txt");
        }

        private static string Convert(byte[] seed)
        {
            var hmac = new Extra.Utilities.HMAC();
            hmac.DoInit(tmdbKey);
            hmac.DoUpdate(seed, 0, seed.Length);

            byte[] hash = hmac.DoFinalButGetHash();

            return BitConverter.ToString(hash).Replace("-", "");
        }

        private static string FetchXML(string url)
        {
            var xml = new XmlDocument();
            try
            {
                xml.Load(url);
            }
            catch (Exception e) when (e is WebException || e is XmlException)
            {
                return "";
            }

            XmlNode node = xml.DocumentElement.SelectSingleNode("name");
            string result = node.InnerText.Replace("\r", "").Replace("\n", " ").Replace(";", " ");
            return result.Replace("!", " ").Replace("\"", " ");
        }

        private static string FetchUpdateTitle(string titleId)
        {
            string url = "https://a0.ww.np.dl.playstation.net/tpl/np/" + titleId + "/" + titleId + "-ver.xml";

            var xml = new XmlDocument();
            try
            {
                xml.Load(url);
            }
            catch (Exception e) when (e is WebException || e is XmlException)
            {
                return "unknown";
            }

            XmlNode node = xml.DocumentElement.SelectSingleNode("//*[name() = 'TITLE']");
            string result = node.InnerText.Replace("\r", "").Replace("\n", " ").Replace(";", " ");
            return result.Replace("!", " ").Replace("\"", " ");
        }
    }
}
