﻿using System;
using System.Security.Cryptography;

namespace Extra.Utilities
{
    internal class HMAC
    {
        private HMACSHA1 mac;
        private byte[] result;

        public bool DoFinal(byte[] expectedhash, int hashOffset)
        {
            return CryptographicEngines.CompareBytes(result, 0, expectedhash, hashOffset, expectedhash.Length);
        }

        public byte[] DoFinalButGetHash()
        {
            return result;
        }

        public void DoInit(byte[] key)
        {
            try
            {
                mac = new HMACSHA1(key);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void DoUpdate(byte[] i, int inOffset, int len)
        {
            result = mac.ComputeHash(i, inOffset, len);
        }
    }
}
